package ru.gb.pages;

import io.qameta.allure.Step;
import ru.gb.locators.FormPageLocators;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class FormPage {

    private FormPageLocators locator(){
        return new FormPageLocators();
    }

    @Step("Вводим текст в поле ввода")
    public FormPage writeText(String text){
        $(locator().fieldInput()).setValue(text);
        return this;
    }

    @Step("Проверка на соответствие напечатанного текста")
    public FormPage checkWhatTextRight(String yourText){
        $(locator().fieldResultInput()).is(text(yourText));
        return this;
    }

    @Step("Кликаем на кнопку свитча")
    public FormPage clickSwitchButton(){
        $(locator().switchButton()).click();
        return this;
    }

    @Step("Проверка на появление нужного текста под свитчем")
    public FormPage checkSwitchText(String switchText){
        $(locator().switchMessage()).is(text(switchText));
        return this;
    }

}
