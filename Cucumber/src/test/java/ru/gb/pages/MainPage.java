package ru.gb.pages;

import io.qameta.allure.Step;
import ru.gb.locators.MainPageLocators;

import static com.codeborne.selenide.Selenide.$;

public class MainPage {

    private MainPageLocators locator(){
        return new MainPageLocators();
    }
    @Step("Кликаем по кнопке логина в меню и переходим на новую строницу логина")
    public LoginPage clickLoginMenuButton(){
        $(locator().menuLoginButton()).click();
        return new LoginPage();
    }

    @Step("Кликаем по кнопке формы в меню и переходим на новую страницу формы")
    public FormPage clickMenuFormButton(){
        $(locator().menuFormButton()).click();
        return new FormPage();
    }

}
