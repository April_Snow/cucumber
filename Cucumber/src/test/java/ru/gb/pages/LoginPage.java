package ru.gb.pages;

import io.qameta.allure.Step;
import ru.gb.locators.LoginPageLocators;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    private LoginPageLocators locator(){
        return new LoginPageLocators();
    }
    @Step("Кликаем на кнопку логин")
    public LoginPage clickLoginButton(){
        $(locator().loginButton()).click();
        return this;
    }
    @Step("Проверка сообшения об ошибки при пустом поле пароль")
    public LoginPage checkErrorEmptyPasswordInputMessage(String errorText){
        $(locator().errorEmptyPasswordInputMessage()).is(text(errorText));
        return this;
    }
    @Step("Вводим имейл")
    public LoginPage setEmail(String email){
        $(locator().emailInput()).setValue(email);
        return this;
    }

    @Step("Вводим пароль")
    public LoginPage setPassword(String password){
        $(locator().passwordInput()).setValue(password);
        return this;
    }

    @Step("Проверка сообшения об ошибки при пустом поле имейла")
    public LoginPage checkErrorEmptyEmailInputMessage(String errorMailText) {
        $(locator().errorEmptyEmailMessage()).is(text(errorMailText));
        return this;
    }
}
