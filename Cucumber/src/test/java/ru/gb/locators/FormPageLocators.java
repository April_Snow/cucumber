package ru.gb.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class FormPageLocators {

    public By fieldInput(){
        return MobileBy.AccessibilityId("text-input");
    }

    public By fieldResultInput(){
        return MobileBy.AccessibilityId("input-text-result");
    }

    public By switchButton(){
        return MobileBy.AccessibilityId("switch");
    }

    public By switchMessage(){
        return MobileBy.AccessibilityId("switch-text");
    }

}
