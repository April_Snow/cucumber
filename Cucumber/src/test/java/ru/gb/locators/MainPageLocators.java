package ru.gb.locators;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class MainPageLocators {

    public By menuLoginButton (){
        return  MobileBy.xpath("//android.view.ViewGroup[@content-desc=\"Login\"]/android.view.ViewGroup/android.widget.TextView");
    }

    public By menuFormButton(){
        return MobileBy.xpath("//android.view.ViewGroup[@content-desc=\\\"Forms\\\"]/android.view.ViewGroup/android.widget.TextView");
    }
}
