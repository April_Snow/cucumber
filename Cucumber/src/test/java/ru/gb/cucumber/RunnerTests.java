package ru.gb.cucumber;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = {"src/test/resources/features"}, glue = {"ru/gb/cucumber/steps"})
public class RunnerTests extends AbstractTestNGCucumberTests {
}
