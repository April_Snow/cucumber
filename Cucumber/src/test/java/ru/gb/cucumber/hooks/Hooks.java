package ru.gb.cucumber.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;

import static com.codeborne.selenide.Selenide.closeWebDriver;

public class Hooks {

    @Before
    public void prepareDate(){
        System.out.println("Hello cucumber");
    }

    @After
    public void clearDate(){
        System.out.println("Bye bye cucumber");
        closeWebDriver();
    }
}
