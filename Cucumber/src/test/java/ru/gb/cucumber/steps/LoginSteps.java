package ru.gb.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ru.gb.base.BaseTest;
import ru.gb.pages.LoginPage;

public class LoginSteps extends BaseTest {

    LoginPage loginPage;

    @Given("User is on Login page")
    public void userIsOnLoginPage(){
       loginPage = openApp()
               .clickLoginMenuButton();
    }

    @When("User clicks login button")
    public void userClickLoginButton() {
        loginPage.clickLoginButton();
    }

    @Then("User should see error message {string}")
    public void userShouldSeeErrorMessage(String string)  {
        loginPage.checkErrorEmptyEmailInputMessage(string);
    }

    @When("User enters password")
    public void userEntersPassword(String password) {
        loginPage.setPassword(password);
    }

    @When("User enters email")
    public void userEntersEmail(String email) {
        loginPage.setEmail(email);
    }

}
