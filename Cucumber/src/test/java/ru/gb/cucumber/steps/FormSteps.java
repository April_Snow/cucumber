package ru.gb.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ru.gb.base.BaseTest;
import ru.gb.pages.FormPage;

public class FormSteps extends BaseTest {

    FormPage formPage;
    @Given("User is on Form page")
    public void userIsOnFormPage() {
        formPage = openApp()
                .clickMenuFormButton();
    }

    @When("User enters text")
    public void userEntersText(String text) {
        formPage.writeText(text);
    }
    @Then("User should see printed text")
    public void userShouldSeePrintedText(String text) {
        formPage.checkWhatTextRight(text);
    }

    @When("User clicks on switch")
    public void userClicksOnSwitch() {
        formPage.clickSwitchButton();
    }

    @Then("User should see {string}")
    public void userShouldSee(String string) {
        formPage.checkSwitchText(string);
    }
}
