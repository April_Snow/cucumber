package ru.gb.base;

import com.codeborne.selenide.WebDriverRunner;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import ru.gb.pages.MainPage;

import java.net.MalformedURLException;
import java.net.URL;

import static jdk.internal.net.http.common.Utils.close;

public class BaseTest {

    public MainPage openApp() {
        WebDriver driver = null;
        try {
            driver = getAndroidDriver();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Opps, we have problems with URL for driver!");
        }
        WebDriverRunner.setWebDriver(driver);
        return new MainPage();
    }
    private WebDriver getAndroidDriver() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("app", "/Users/april/Downloads/Android-NativeDemoApp-0.2.1.apk");

        return new AndroidDriver(new URL("http://192.168.99.190:4444/wd/hub"), capabilities);
    }

    @AfterTest
    public void setDown(){
        close();
    }
}
