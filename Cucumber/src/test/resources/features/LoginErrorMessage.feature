Feature: Login

  Scenario: Login without email and password
    Given User is on Login page
    When User clicks login button
    Then User should see error message "Please enter a valid email address"

  Scenario: Login without email
    Given User is on Login page
    When User enters password
    And User clicks login button
    Then User should see error message "Please enter a valid email address"

  Scenario: Login without password
    Given User is on Login page
    When User enters email
    And User clicks login button
    Then User should see error message "Please enter at least 8 characters"